/* 
 * kanji paint
 * THis is an application that allows to paint a kanji in a canvas and them convert that draw into an image png, jpg..etc
 */


(function(){
    
    var Utils = {};
    
    
    Utils.updateProperties = function(options) {
        for (var property in options) {
            //update only the properties that already exists in this object
            if ( options.hasOwnProperty(property) && this[property] !== undefined ) {
              if ( options[ property ] ) {
                this[ property ] = options[ property ];
              }
            }
        }
    };
    
    //==========================================================================================
    //====================       error     =====================================================
    //==========================================================================================
    
    var Error = function(  message ) {
        this.message = message;
        this.name = "Exception";
        this.toString = function(){
            return this.name + ' ;  ' + this.message;
        };
    };
    
    
    //===========================================================================================
    //====================      Layer      ======================================================
    //===========================================================================================
   
   
    //***************************  CONSTRUCTOR   *******************************
    
    var Layer = function(config){
        
        var continuar = true;
        var error = new Error();
        
        //config options
        var options = {
            id: null,
            canvas: null,
            context: null
        };
        
        //options validation
        if( typeof config !== null ) {
            if ( typeof config === 'string'  ) {
                options.id = config;
            } 
            else if ( typeof config === 'object' ) {
                if ( config.hasOwnProperty('id') ) {
                    options.id = config.id;
                }
            }
            else {
                continuar = false;
            }
        } else {
            continuar = false;
        }
        
        if ( !continuar ) {
            error.name = " incompleateParameters ";
            error.message = " you need to pass this parameters on the options config { id }";
            throw error;
        }
        
        //create the new canvas object
        options.canvas = document.createElement('canvas');
        options.canvas.id = options.id;
        options.canvas.style.top = 0;
        options.canvas.style.left = 0;
        options.canvas.style.width = '100%';
        options.canvas.style.height = '100%';
        options.canvas.style.position = 'absolute';
        //canvas.width = 500;
       //canvas.height = 500;

        //verity if canvas is supported for this browser
        if (options.canvas.getContext){
            options.context = options.canvas.getContext('2d');
        } else {
            error.name = "canvasNotSupported";
            error.message = "your web browser doesn't support canvas";
            throw error;
        }
        
        //update properties
        Utils.updateProperties.call( this, options );
        
        
    };
    
    Layer.prototype.constructor = Layer;
    
    
    
    //**************************   PROPERTIES   ********************************
    
    Layer.prototype.id = '';
    Layer.prototype.canvas = null;
    Layer.prototype.context = null;
    
    
    
    
    //=======================================================================================================
    //====================   KanjiPaint =====================================================================
    //=======================================================================================================
    
    /**
     * kanjiPaint private methods and properties container
     * @type object
     */
    var kpm = {};
    
    //***************************  CONSTRUCTOR   *******************************
    
    //constructor
    var KanjiPaint = function( config ){
        
        var continuar = true;
        var error = new Error();
               
        //update config properties
        Utils.updateProperties.call( this, config );

        
        //createContainer
        this.container = document.createElement('div');
        this.container.id = this.id;
        this.container.style.position = 'relative';
        this.container.style.width = this.width;
        this.container.style.height = this.height;       
        
        // render container if its possible
        if ( this.renderTo ){
            this.render();
        }
        
        //draw the main layer
        kpm.initLayers.call( this );
        

        
    };
    
    //assigning the constructor method to prototype
    KanjiPaint.prototype.constructor = KanjiPaint;
    
    
    //**************************   PROPERTIES   ********************************
    
    KanjiPaint.prototype.id = '';
    KanjiPaint.prototype.renderTo = '';
    
    /**
     * @property {element} container the element where the kanjipaint will be draw
     */
    KanjiPaint.prototype.container = '';
    KanjiPaint.prototype.width = '';
    KanjiPaint.prototype.height = '';
    KanjiPaint.prototype.mainLayer = null;
    KanjiPaint.prototype.debugLayer = null;
    KanjiPaint.prototype.debug = false;
    
    kpm.mouse = {
            x: 0,
            y: 0,
            lastPosition: {
                x: 0,
                y: 0
            },
            pressed: false
        }
    
    //**************************   METHODS   ********************************

    /**
     * @public
     * @param {string} target the id of the element where to draw 
     */
    KanjiPaint.prototype.render = function( target ) {
        var error = new Error();
        try {
            if ( typeof target === 'string' ) {
                document.getElementById( target ).insertBefore( this.container );
            } else {
                document.getElementById( this.renderTo ).insertBefore( this.container );
            }
        } catch(e){
            var t = target || this.renderTo;
            error.name = "cannotRender";
            error.message = "It doesn't exist an element with this id : " + t ;
            throw error;
        }
    };
    
    /**
     * @private
     */
    kpm.initLayers = function(){
        this.mainLayer =  new Layer('layer_main');
        this.container.insertBefore( this.mainLayer.canvas );
        this.debugLayer =  new Layer('layer_debug');
        this.container.insertBefore( this.debugLayer.canvas );
        if ( !this.debug ) {
            this.debugLayer.style.display = 'none';
        }
    };
    
    kpm.initialSetup = function(){
        
        var canvas = this.mainLayer.canvas;
        
        
        kpm.paintPosition = { x: 0, y: 0 } ;
        
        /***************** event listeners setting *****************/
        
        canvas.addEventListener( 'mousedown', function( event ){
            mouse.pressing = true;
            writemessage( canvas, 'mousedown');
            
            //position donde comenzaré a dibujar
            var position = captureMousePosition( canvas, event );
            paintPosition.x = position.x;
            paintPosition.y = position.y;
            
        });
        
        canvas.addEventListener( 'mouseup', function(){
            mouse.pressing = false;
            writemessage( canvas, 'mouseup');
            
            //resetear buffered
            buffered = [];
        });
        
        //capturar posicion del mouse cada vez que se mueva
        canvas.addEventListener( 'mousemove', function (event) {
            /*
            var position = captureMousePosition( canvas, event );
            
            
            //posicion vieja
            mouse.lastPosition.x = mouse.x;
            mouse.lastPosition.y = mouse.y;
            
            //posicion nueva
            mouse.x = position.x;
            mouse.y = position.y;
        
            
            
            
            if ( mouse.pressing ) {
                
                eventCont++;
                
                //guardar en el buffer
                buffered[ buffered.length ] = { x: x, y: y, e: eventCont } ;
                buffer[ buffer.length ] = { x: x, y: y, e: eventCont } ;
                
            }*/
            
        },false );
    };
    
    /**
     * @private
     * @param {canvas.context} context
     * @param {string} message
     */
    kpm.writemessage  = function( context, message ){
        context.save();
        context.clearRect( 100,0, 150,20 );
        context.font = 'normal 14px Calibri';
        context.fillStyle = 'black';
        context.fillText( message, 100, 10 );
        context.restore();
      };
      
    kpm.marcar = function( x, y, radius, color, e ) {
      context.save();
      context.beginPath();
      context.arc(x, y, radius, 0 , 2 * Math.PI, false);
      context.fillStyle = color;
      context.fill();
      context.lineWidth = 1;
      context.strokeStyle = color ;
      context.stroke();
      context.restore();
    };

    
    
    //==========================================================================
    //===========  Adding the classes to window scope   ========================
    //==========================================================================
    
    window.KanjiPaint = KanjiPaint;
    window.Layer = Layer;
    
}());