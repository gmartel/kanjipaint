

(function(namespace){

/**
 * @author Guillermo Martel <martelgames@yahoo.com>
 * 
 * captureMousePosition
 *
 * Returns the position x, y of the mouse relative to the passed element
 * @param {element} element DOM element object
 * @param {event} event ,DOM event object
 * @return object { x: 45, y: 129 }
 */
var captureMousePosition = function( element, event ) {
    
    var x = 0;
    var y = 0;
    
    if ( event.pageX || event.pageY ) {
        x = event.pageX;
        y = event.pageY;
    } else {
        x = event.clientX + document.body.scrollLeft +
            document.documentElement.scrollLeft;
        y = event.clientY + document.body.scrollTop +
            document.documentElement.scrollTop;
    }

    x = x - element.offsetLeft;
    y = y -  element.offsetTop;
        
    return { x: x, y: y };
};


namespace.captureMousePosition = captureMousePosition;

}(this));