/* 
 * THe sketch class for kanjipaint
 */


(function(namespace){
    'use strict';
    
    //***************************  CONSTRUCTOR   *******************************
    
    //constructor
    var Sketch = function( config ){
        
        var continuar = true;
        var error = new KanjiPaint.Error();
               
        //update config properties
        updateProperties.call( this, config );
        updateBrushesRootPath.call(this);

        //createContainer
        this.container = document.createElement('div');
        this.container.id = this.id;
        this.container.style.position = 'relative';
        this.container.style.width = this.width + 'px';
        this.container.style.height = this.height + 'px';       
        
        // render container if its possible
        if ( this.renderTo ){
            this.render();
        }
        
        //draw the main layer
        initLayers.call( this );
        
        //initial setup
        initialSetup.call( this );
        

        startLoop.call( this, drawLoop, 1000/60 );
        
    };
    
    //assigning the constructor method to prototype
    Sketch.prototype.constructor = Sketch;
    
    
    //**************************   PROPERTIES   ********************************
    
       //----- publics
    
    Sketch.prototype.id = '';
    
    /**
     * @property {string} renderTo element id where to draw the sketch
     */
    Sketch.prototype.renderTo = '';
    
    
    /**
     * @property {element} container the element where the kanjipaint will be draw
     */
    Sketch.prototype.container = '';
    Sketch.prototype.width = '500';
    Sketch.prototype.height = '500';
    Sketch.prototype.mainLayer = null;
    Sketch.prototype.debugLayer = null;
    Sketch.prototype.debug = false;
    Sketch.prototype.defaultBrush = 'DEFAULT';
    /**
     * @property {string} path path to de folder that KanjiPaint relative to the html where the instance is
     */
    Sketch.prototype.path = './';
    
    
     //----- privates
    
    var loopConfig = {
        continue : true,
        lastTime: 0
    };
    
    var mouse = {
        x: 0,
        y: 0,
        lastPosition: {
            x: 0,
            y: 0
        },
        downPosition: {
            x: 0,
            y: 0
        },
        pressing: false
    };
    
    var BRUSHES = {
        DEFAULT: { src: 'img/brushes/brush1.png', offsetx: -8, offsety: 5 }
    };
    
    var buffer = [];
    var buffered = [];
    var painted = [];
    var bufferDeBuffered = [];
    var eventCont = 0;
    
    //**************************   METHODS   ********************************

    /**
     * @public
     * @param {string} target the id of the element where to draw 
     */
    Sketch.prototype.render = function( target ) {// console.log( document.getElementById('bloque') );
        var error = new KanjiPaint.Error(); 
        try {
            if ( typeof target === 'string' ) {
                namespace.document.getElementById( target ).insertBefore( this.container, null );
            } else {
                namespace.document.getElementById( this.renderTo ).insertBefore( this.container, null );
            }
        } catch(e){
            var t = target || this.renderTo;
            error.name = "cannotRender";
            error.message = "It doesn't exist an element with this id : " + t ;
            throw error;
        }
    };
    
    Sketch.prototype.destroy = function(){
       stopLoop();
       var node = document.getElementById( this.renderTo );
       if ( node.parentNode ) {
          node.parentNode.removeChild(node);
       }
    };
    
    /**
     * converts the draw on the sketch into a png image
     * @returns {image element png}
     */
    Sketch.prototype.toImage = function() {
       
        return canvasToImage( this.mainLayer.canvas );
    };
    
    
    /**
     * update the properties of this object whit the properties of the passed 
     * options properties that are not null and that exists on the layer object.
     * 
     * @private
     * @param {object} options
     */
     function updateProperties(options) {
        for (var property in options) {
            //update only the properties that already exists in this object
            if ( options.hasOwnProperty(property) && this[property] !== undefined ) {
              if ( options[ property ] ) {
                this[ property ] = options[ property ];
              }
            }
        }
    };
    
    /**
     * @private
     */
    function initLayers(){
        var este = this;
        this.mainLayer =  new KanjiPaint.Layer({ id:  este.id + '_layer_main', width: parseInt(this.width) , height: parseInt(this.height)  });
        this.container.insertBefore( this.mainLayer.canvas , null );
        this.debugLayer =  new KanjiPaint.Layer({ id: este.id + '_layer_debug', width: parseInt(this.width) , height: parseInt(this.height)  });
        this.container.insertBefore( this.debugLayer.canvas , null  );
        if ( !this.debug ) {
            var display = this.debugLayer.canvas.getAttribute( 'style' );
            this.debugLayer.canvas.setAttribute( 'style', display + ' display:none;' );
        }
    };
    
    /**
     * @private
     */
    function initialSetup(){

        var sketch = document.getElementById(this.id);
        var canvas = this.mainLayer.canvas;
        var context = this.mainLayer.context;
        
        var brush = new Image();
        var brushName = this.defaultBrush;
        
        /***************** event listeners setting *****************/
        
        canvas.addEventListener( 'mousedown', function( event ){
            mouse.pressing = true;
            //writemessage( context , 'mousedown');
            
            //position donde comenzaré a dibujar
            var position = captureMousePosition( sketch, event );
            mouse.downPosition.x = position.x;
            mouse.downPosition.y = position.y;
            
            mouse.x = position.x;
            mouse.y = position.y;
            
            //draw a point in this position
            if ( mouse.pressing ) { 
                brush = new Image();
                brush.src = BRUSHES[brushName].src;
                context.moveTo( mouse.downPosition.x, mouse.downPosition.y );
                context.drawImage( brush, ( parseInt(mouse.downPosition.x) + parseInt(BRUSHES[brushName].offsetx) ) , (parseInt(mouse.downPosition.y) + parseInt(BRUSHES[brushName].offsety)) ) ; 
                
            }
        });
        
        canvas.addEventListener( 'mouseup', function(){
            mouse.pressing = false;
            //writemessage( context , 'mouseup');
            
            //resetear buffered
            buffered = [];
        });
        
        //capturar posicion del mouse cada vez que se mueva
        canvas.addEventListener( 'mousemove', function (event) {
            
            var position = captureMousePosition( sketch, event );

            //posicion vieja
            mouse.lastPosition.x = mouse.x;
            mouse.lastPosition.y = mouse.y;
            
            //posicion nueva
            mouse.x = position.x;
            mouse.y = position.y;

            if ( mouse.pressing ) {
                
                eventCont++;
                
                //guardar en el buffer
                buffered[ buffered.length ] = { x: mouse.x ,y: mouse.y, e: eventCont } ;
                buffer[ buffer.length ] = { x: mouse.x, y: mouse.y, e: eventCont } ;
                
            }
            
        },false );
    };

    
    function stopLoop(){
        loopConfig.continue = false;
    }
    
    function animation( method, milliseconds ){

        var este = this;
        var now = new Date().getTime();
        var diff = now - loopConfig.lastTime;

        if ( diff >= milliseconds ){                    

            loopConfig.lastTime = now;    
            method.call(este);
        }

        if ( loopConfig.continue ) { 
            namespace.requestAnimationFrame( function(){
                animation.call( este,  method, milliseconds );
            } );
        }

    } //animation
    
    function startLoop(  method, milliseconds ){
        
        loopConfig.continue = true;
        animation.call( this, method, milliseconds );
        
    };//startLoop
    
    
    /**
     * This method  will be draw something  on the scketch, this is called by startLoop,
     * so it will be draw something every x milliseconds
     * 
     * @private
     */    
    function drawLoop(){
        
            var brush = new Image();
            var brushName = this.defaultBrush;
            var context = this.mainLayer.context;

                       
            
            if ( mouse.pressing && buffered.length > 0 ) {
                
                //marcar inicio
                if ( this.debug ) marcar( mouse.downPosition.x, mouse.downPosition.y, 3, 'green' );
                
                //context.save();
                //dibujar brush
                brush = new Image();
                brush.src =  BRUSHES[brushName].src;
                context.moveTo( mouse.downPosition.x, mouse.downPosition.y );
                var length = buffered.length;
                var bufferedTemp = buffered.slice();
                
                var recta = new RectaDosPuntos();
                var last = {
                    x: mouse.downPosition.x,
                    y: mouse.downPosition.y
                };
                
                
      
                for ( var x = 0; x < length ; x++ ) {
                    
                    //context.drawImage( brush, bufferedTemp[x].x, bufferedTemp[x].y );

                    if ( x === 0 ) {
                        //*especial
                        //volver a correr esto en un while
                        // desde last position hasta paint position
                        recta.p2.x = bufferedTemp[x].x;
                        recta.p2.y = bufferedTemp[x].y;
                        recta.p1.x = last.x;
                        recta.p1.y = last.y;
                        
                        
                        
                    } else {
                        recta.p1.x = bufferedTemp[x - 1].x;
                        recta.p1.y = bufferedTemp[x - 1].y;
                        recta.p2.x = bufferedTemp[x].x;
                        recta.p2.y = bufferedTemp[x].y;
                    }
                     
                        //mover brush de p1 a p2
                        //p1.y = p2.y
                        var px = recta.p1.x;
                        var py = recta.p1.y;
                        var incremento = 1;
                        var continuar = true;
                        while ( continuar ) {
                            //direccion : indica hacia donde se mueve el puntero derecha, izquierda, arriba, abajo
                            var direccionx = 1;
                            var direcciony = 1;
                            
                            //ajustar la direccion
                            ( recta.p2.x > recta.p1.x ) ? direccionx = 1 : direccionx = -1;
                            ( recta.p2.y > recta.p1.y ) ? direcciony = 1 : direcciony = -1;
                            
                            //ajustar la posicion
                            if ( recta.p1.y === recta.p2.y ) {
                                px = px + incremento * direccionx;
                            } else
                            if ( recta.p1.x === recta.p2.x ) {
                                py = py + incremento * direcciony ;
                            } else {
                                px = px + incremento * direccionx;
                                py = recta.resolverY( px );
                            }
                            
                            //dibujar brush
                            // ajustar posicion de salida para que cuadre
                            // con la la forma del pincel
                            context.drawImage( brush, ( parseInt(px) + parseInt(BRUSHES[brushName].offsetx) ) , (parseInt(py) + parseInt(BRUSHES[brushName].offsety)) ) ; 
                            
                            // comenzando en punto1,
                            // continuar hasta alcanzar el punto 2.
                            if ( direccionx === 1 &&  direcciony === 1 ) {
                                continuar = ( px <= recta.p2.x && py <= recta.p2.y ) === true;
                            }
                            if ( direccionx === 1 &&  direcciony === -1 ) {
                                continuar = ( px <= recta.p2.x && py >= recta.p2.y ) === true;
                            }
                            if ( direccionx === -1 &&  direcciony === 1 ) {
                                continuar = ( px >= recta.p2.x && py <= recta.p2.y ) === true;
                            }
                            if ( direccionx === -1 &&  direcciony === -1 ) {
                                continuar = ( px >= recta.p2.x && py >= recta.p2.y ) === true;
                            }
                        }
                            
                        //context.drawImage(brush, bufferedTemp[x].x, bufferedTemp[x].y );
                   
                   
                        
                        painted[ painted.length ] = { x: bufferedTemp[x].x, y: bufferedTemp[x].y } ;
                        buffered.shift();
                    
                    
                        last.x = bufferedTemp[x].x;
                        last.y = bufferedTemp[x].y;
                        
                    //}
                }
            
                
                //mouse.lastPosition.x = bufferedTemp[ bufferedTemp.length - 1 ].x;
                //mouse.lastPosition.y = bufferedTemp[ bufferedTemp.length -1 ].y;
                
                mouse.downPosition.x = bufferedTemp[ bufferedTemp.length - 1 ].x;
                mouse.downPosition.y = bufferedTemp[ bufferedTemp.length -1 ].y;
                
                //bufferDeBuffered[ bufferDeBuffered.length ] = bufferedTemp.slice();
                
                //marcar final
                if ( this.debug ) marcar( mouse.downPosition.x, mouse.downPosition.y, 3, 'blue' );
                
                //buffered = [];  
            }
    };
    
    
    /**
     * converts the given canvas into a png image
     * @param {canvas} canvas canvas element
     * @returns {image element}
     */
    function canvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        return image;
    };
    
    /**
     * @private
     * @param {canvas.context} context , a canvas 2d context object
     * @param {string} message message to write
     */
    function writemessage ( context, message ){
        //var context = canvas.getContext( '2d');
        context.save();
        context.clearRect( 100,0, 150,20 );
        context.font = 'normal 14px Calibri';
        context.fillStyle = 'black';
        context.fillText( message, 100, 10 );
        context.restore();
    };
    
    function marcar( context, x, y, radius, color, e ) {
        context.save();
        context.beginPath();
        context.arc(x, y, radius, 0 , 2 * Math.PI, false);
        context.fillStyle = color;
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = color ;
        context.stroke();
        context.restore();
    };
    
    /**
     * update de brushes path with the path given in the configuration on the constructor
     * @private
     */
    function updateBrushesRootPath(){
        var temp = {};
        var path = this.path;
        
        if ( path[path.length-1] === '/' ) {
            path = path.substring(0, path.length-1 );
        }
        
        for( var m in BRUSHES ) {
            temp[m] = BRUSHES[m];
            temp[m].src = path + '/' + BRUSHES[m].src;
        }
        BRUSHES = temp;
    }

    /**
     * @class RectaDosPuntos
     * @private
     * 
     * esta clase sirve para dibujar y calcualar una recta usando 2 puntos conocidos
     */
    function RectaDosPuntos(){};
         RectaDosPuntos.prototype.constructor = RectaDosPuntos;
         RectaDosPuntos.prototype.p1 = { x: 0, y: 0 };
         RectaDosPuntos.prototype.p2 = { x: 0, y: 0 };
         RectaDosPuntos.prototype.resolverY = function ( x ) {
             return (( x - this.p1.x ) * ( this.p2.y - this.p1.y ))/( this.p2.x - this.p1.x) + this.p1.y;
         };
         RectaDosPuntos.prototype.resolverX = function( y ) {
             return (( y - this.p1.y ) * ( this.p2.x -this.p1.x )) / ( this.p2.y - this-p1.y ) + this.p1.x;
         };
    
    //==========================================================================
    //===========  Adding the classes to namespace scope   ========================
    //==========================================================================
    
    namespace.KanjiPaint = namespace.KanjiPaint || {};
    namespace.KanjiPaint.Sketch = Sketch;
}(this));