/* 
 * Error class for kanjipaint
 */

(function(namespace){
  
    var Error = function(  message ) {
        this.message = message;
        this.name = "Exception";
        this.toString = function(){
            return this.name + ' ;  ' + this.message;
        };
    };
    
    //**************************************************************************
    
    namespace.KanjiPaint = window.KanjiPaint || {};
    namespace.KanjiPaint.Error = Error;
}(this));

