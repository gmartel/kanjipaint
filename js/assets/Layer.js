/* 
 * Layer class interfaz for kanjipaint
 * This abstracts the canvas element, creating a canvas and assiging it to this 
 * layer object.
 * Its also verify if the browser supports canvas2d context.
 */

(function(namespace){
    
    
       
    //***************************  CONSTRUCTOR   ****************************************************************************************************
    
    var Layer = function(config){
        
        var continuar = true;
        var error = new namespace.KanjiPaint.Error();
        
        //config options
        var options = {
            id: null,
            canvas: null,
            context: null,
            width: 800,
            height: 600
        };
        
        //options validation
        if( typeof config !== null ) {
            if ( typeof config === 'string'  ) {
                options.id = config;
            } 
            else if ( typeof config === 'object' ) {
                if ( config.hasOwnProperty('id') ) {
                    options.id = config.id;
                }
                if ( config.hasOwnProperty('width') ) {
                    options.width = config.width;
                }
                if ( config.hasOwnProperty('height') ) {
                    options.height = config.height;
                }
            }
            else {
                continuar = false;
            }
        } else {
            continuar = false;
        }
        
        if ( !continuar ) {
            error.name = " incompleateParameters ";
            error.message = " you need to pass this parameters on the options config { id }";
            throw error;
        }
        
        //create the new canvas object
        options.canvas = document.createElement('canvas');
        options.canvas.id = options.id;
        options.canvas.style.top = 0;
        options.canvas.style.left = 0;
        options.canvas.style.position = 'absolute';
        options.canvas.width = options.width;
        options.canvas.height = options.height;

        //verity if canvas is supported for this browser
        if (options.canvas.getContext){
            options.context = options.canvas.getContext('2d');
        } else {
            error.name = "canvasNotSupported";
            error.message = "your web browser doesn't support canvas";
            throw error;
        }
        
        //update properties
        updateProperties.call( this, options );
        
        
    };
    
    Layer.prototype.constructor = Layer;
    
    
    
    //**************************   PROPERTIES   ****************************************************************************************************
    
    
    Layer.prototype.id = '';
    Layer.prototype.canvas = null;
    Layer.prototype.context = null;
    
    
    //************************* METHODS ****************************************************************************************************
    
    /**
     * update the properties of this object whit the properties of the passed 
     * options properties that are not null and that exists on the layer object.
     * 
     * @private
     * @param {object} options
     */
    updateProperties = function(options) {
        for (var property in options) {
            //update only the properties that already exists in this object
            if ( options.hasOwnProperty(property) && this[property] !== undefined ) {
              if ( options[ property ] ) {
                this[ property ] = options[ property ];
              }
            }
        }
    };
    
    
    //********************************************************************************************************************************************
    
    namespace.KanjiPaint = window.KanjiPaint || {};
    namespace.KanjiPaint.Layer = Layer;
    
}(this));

